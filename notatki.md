npm i -g typescript 
tsc --init


npm i -g create-react-app
npx create-react-app nazwa_projektu_katalogu --template typescript


# Jest
https://jestjs.io/docs/en/expect

## lista testów
npm test -- --verbose

--testMatch                   
The glob patterns Jest uses to detect test files.   

--testNamePattern, -t         
Run only tests with a name that matches the
                  regex pattern. 
                                
 --testPathPattern             
 A regexp pattern string that is matched against
  all tests paths before executing the test.

# React ext
  https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets


https://testing-library.com/docs/react-testing-library/api
https://testing-library.com/docs/queries/about/
https://testing-library.com/docs/queries/bytitle
https://github.com/testing-library/jest-dom


# Snapshot testing
https://jestjs.io/docs/en/snapshot-testing
yarn add @types/react-test-renderer react-test-renderer


# JSDOM
https://github.com/jsdom/jsdom
https://github.com/jsdom/jsdom#unimplemented-parts-of-the-web-platform


# ts-jest
https://kulshekhar.github.io/ts-jest/docs/test-helpers

# Debugger in tests
https://create-react-app.dev/docs/debugging-tests/


# Act
// Warning: An update to PlaylistsView inside a test was not wrapped in act(...)
https://reactjs.org/docs/test-utils.html#act
https://github.com/threepointone/react-act-examples/blob/master/sync.md 
https://kentcdodds.com/blog/fix-the-not-wrapped-in-act-warning