import React from 'react';
import { SearchView } from './search/containers/SearchView';

function App() {
  return (
    <div className="App">
      {/* .container>.row>.col */}
      <div className="container">
        <div className="row">
          <div className="col">
            <h1>Hello React</h1>
            <SearchView />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
