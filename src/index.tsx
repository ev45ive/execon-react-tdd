import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.css';


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


// window.React = React;
// window.ReactDOM = ReactDOM;

// const div = React.createElement('div', {
//   id: '123', className: 1 < 2 ? 'test' : '', style: { color: color }
// },
//   React.createElement('span', {}, 'Ala ma'),
//   React.createElement('input', {}),
//   React.createElement('span', {}, 'i papuge')
// )

// const color = 'blue'
// type Props = {
//   color: string;
//   val: number;
//   pet: string;
// };

// // const PersonInfo = (params?:any) => <div id="123" className={1 < 2 ? 'test' : ''} style={ {color: params.color} }>
// const PersonInfo = (props?: Props) => props ? <div id="123" className={1 < props.val ? 'test' : ''} style={{ color: props.color }}>
//   <span>Ala ma</span>
//   <input type="text" />
//   <span>i {props.pet}</span>
// </div> : null;

// debugger;

// ReactDOM.render(<div>
//   {PersonInfo({ color: 'red', val: 1, pet: 'dog' })}
//   {/* {PersonInfo({ color: 'green', val: 2, pet: 'parrot' })} */}
//   <PersonInfo color={color} val={2} pet="parrot" />
//   {PersonInfo()}
// </div>, document.getElementById('root'))

