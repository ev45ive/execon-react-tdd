import React, { FC, ReactElement, useState } from 'react'
import { Playlist } from '../../common/model/playlist'
import { PlaylistDetails } from '../components/PlaylistDetails'
import { PlaylistList } from '../components/PlaylistList'
import { playlistData } from './playlistData'


interface Props {

}

export const PlaylistsView: FC<Props> = ({ }) => {
  const [selectedPlaylist, setSelectedPlaylist] = useState<Playlist | undefined>()

  const selectPlaylist = (id: Playlist['id']) => setSelectedPlaylist(
    playlistData.find(p => p.id === id)
  )

  return (
    <div>
      <WrapInBorder>
        <div className="row">
          <div className="col">
            <PlaylistList
              playlists={playlistData}
              selectedId={selectedPlaylist?.id}
              onSelected={selectPlaylist} />

          </div>
          <div className="col">
            {selectedPlaylist ?
              <PlaylistDetails playlist={selectedPlaylist} onEdit={() => { }} /> :
              <p>Please select playlist</p>
            }

          </div>
        </div>



      </WrapInBorder>
    </div>
  )
}


const WrapInBorder: FC = ({ children }) => <div className="awesome-border">
  {children}
</div>