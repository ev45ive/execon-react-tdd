import { act, render, screen } from '@testing-library/react'
import React from 'react'
import { mocked } from 'ts-jest/utils'

import { PlaylistsView } from './PlaylistsView'
import { playlistData } from './playlistData'
import { Playlist } from '../../common/model/playlist'

import { PlaylistEditForm } from '../components/PlaylistEditForm'
import { PlaylistDetails } from '../components/PlaylistDetails'
import { PlaylistList } from '../components/PlaylistList'
jest.mock('../components/PlaylistDetails')
jest.mock('../components/PlaylistList')
jest.mock('../components/PlaylistEditForm')

// jest.mock('../components/PlaylistList', () => ({
//   // __esModule:true, 
//   // default:  jest.fn().mockReturnValue(<div>PlaylistListMOCK</div>)
//   PlaylistList: jest.fn().mockReturnValue(<div>PlaylistListMOCK</div>)
// }))

describe('PlaylistsView', () => {

  const setup = () => {
    //  (PlaylistList as jest.MockedFunction<typeof PlaylistList>)
    //   .mockReturnValue(<div>PlaylistListMOCK</div>)
    mocked(PlaylistList).mockReturnValue(<div>PlaylistListMOCK</div>)
    mocked(PlaylistDetails).mockReturnValue(<div>PlaylistDetailsMOCK</div>)
    mocked(PlaylistEditForm).mockReturnValue(<div>PlaylistEditFormMOCK</div>)

    const { } = render(<PlaylistsView />)
    return {}
  }

  it('shows list of example playlists', () => {
    const { } = setup()

    expect(screen.getByText('PlaylistListMOCK')).toBeInTheDocument()

    expect(PlaylistList).toHaveBeenCalledWith<Parameters<typeof PlaylistList>>({
      playlists: playlistData,
      onSelected: expect.anything()
    }, {}/* Context parameter */)

  })

  it('shows "Please select playlist" when no playlist selected', () => {
    const { } = setup()
    expect(screen.getByText('Please select playlist')).toBeInTheDocument()
  })

  it('shows selected playlist details when playlist selected', () => {
    // act(() => {
      const { } = setup()
    // })
    const props = mocked(PlaylistList).mock.calls[0][0]

    // Warning: An update to PlaylistsView inside a test was not wrapped in act(...)
    // https://reactjs.org/docs/test-utils.html#act
    // https://github.com/threepointone/react-act-examples/blob/master/sync.md 
    act(() => {
      props.onSelected('234')
    })

    expect(screen.getByText('PlaylistDetailsMOCK')).toBeInTheDocument()

    expect(screen.queryByText('Please select playlist')).not.toBeInTheDocument()

    expect(PlaylistDetails).toHaveBeenCalledWith({
      playlist: playlistData[1],
      onEdit: expect.anything()
    }, expect.anything())

  })

  it('shows selected playlist edit form', () => { })
  it('switches between form and details', () => { })
  it('updates list when form is saved', () => { })

})
