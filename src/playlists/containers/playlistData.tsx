import { Playlist } from '../../common/model/playlist';

export const playlistData: Playlist[] = [{
  id: '123',
  name: 'My Playlist 123',
  public: false,
  description: 'Test description',
}, {
  id: '234',
  name: 'My Playlist 234',
  public: false,
  description: 'Test description',
}, {
  id: '345',
  name: 'My Playlist 345',
  public: false,
  description: 'Test description',
}];
