import { render, screen, queryAllByRole, within, queryByAttribute, fireEvent, logRoles } from '@testing-library/react'
import React from 'react'
import { Playlist } from '../../common/model/playlist'
import { PlaylistList } from './PlaylistList'


const playlistData: Playlist[] = [{
  id: '123',
  name: 'My Playlist 123',
  public: false,
  description: 'Test description',
}, {
  id: '234',
  name: 'My Playlist 234',
  public: false,
  description: 'Test description',
}, {
  id: '345',
  name: 'My Playlist 345',
  public: false,
  description: 'Test description',
}]

describe('PlaylistList', () => {
  let containerElem: Element

  const setup = ({
    playlists = playlistData,
    selectedId
  }: {
    playlists?: Playlist[],
    selectedId?: Playlist['id']
  } = {}) => {
    const onSelectedIdSpy = jest.fn()
    const { container, rerender } = render(
      <PlaylistList playlists={playlists} selectedId={selectedId} onSelected={onSelectedIdSpy} />
    )
    containerElem = container;
    return { onSelectedIdSpy, container, rerender }
  }

  it('renders list of playlists', () => {
    setup();
    const list = screen.getByRole('listbox', {})
    // const {queryAllByRole } = within(list)
    const items = queryAllByRole(list, 'option', {})

    expect(items).toHaveLength(3)
    items.forEach((item, index) => {
      const { getByText } = within(item)
      expect(getByText(playlistData[index].name)).toBeInTheDocument()
    })
  })

  it('highlights selected playlist', () => {
    setup({ selectedId: playlistData[1].id });
    const list = screen.getByRole('listbox', {})
    // const selectedItem = queryByAttribute('data-playlist-id', list, '234')
    // const selectedItem = within(list).getByText(playlistData[1].name)
    const selectedItems = queryAllByRole(list, 'option', { selected: true })
    // screen.debug()
    // logRoles(selectedItems[0])
    expect(selectedItems).toHaveLength(1)
    const selectedItem = selectedItems[0]
    expect(selectedItem).toBeInTheDocument()
    expect(selectedItem).toHaveClass('selected')
  })

  // it('selects one playlist on click (state)', () => {
  //   setup({});
  //   const list = screen.getByRole('listbox', {})
  //   const items = queryAllByRole(list, 'option', {})
  //   const selectedItems = queryAllByRole(list, 'option', { selected: true })
  //   expect(selectedItems).toHaveLength(0)
  //   // const selectedItem = queryByAttribute('data-playlist-id', list, '234')
  //   const selectedItem = within(list).getByText(playlistData[1].name)
  //   // fireEvent.click(selectedItem)
  //   selectedItem.click()
  //   expect(selectedItem).toHaveClass('selected')
  //   expect(selectedItem).toHaveAttribute('aria-selected', 'true')
  // })


  it('selects one playlist on click (props)', () => {
    const { onSelectedIdSpy } = setup({});
    const list = screen.getByRole('listbox', {})
    const items = queryAllByRole(list, 'option', {})
    const selectedItems = queryAllByRole(list, 'option', { selected: true })
    expect(selectedItems).toHaveLength(0)
    // const selectedItem = queryByAttribute('data-playlist-id', list, '234')
    const selectedItem = within(list).getByText(playlistData[1].name)
    // fireEvent.click(selectedItem)
    selectedItem.click()

    expect(onSelectedIdSpy).toHaveBeenCalledWith(playlistData[1].id)

  })

  it('should change selection when  selectedId changes', () => {
    const { rerender, onSelectedIdSpy } = setup({});

    const list = screen.getByRole('listbox', {})
    let selectedItems = queryAllByRole(list, 'option', { selected: true })
    expect(selectedItems).toHaveLength(0)

    rerender(<PlaylistList playlists={playlistData} selectedId={'234'} onSelected={onSelectedIdSpy} />)
    selectedItems = queryAllByRole(list, 'option', { selected: true })

    expect(selectedItems).toHaveLength(1)

    expect(selectedItems[0]).toBeInTheDocument()
    expect(selectedItems[0]).toHaveClass('selected')
  })

  // it.todo('unselects selected playlist on click')
})
