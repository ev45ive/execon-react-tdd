import React, { useState } from 'react'
import { Playlist } from '../../common/model/playlist'

interface Props {
  selectedId?: Playlist['id']
  playlists: Playlist[]
  onSelected(id: Playlist['id']): void
}

// export const PlaylistList = (props: Props) => {
//   const [selectedId, setSelectedId] = useState(props.selectedId)

//   return (
//     <div>
//       <div role="listbox">
//         {props.playlists.map(p => <div
//            onClick={() => setSelectedId(p.id)}
//           aria-selected={selectedId === p.id}
//           className={selectedId === p.id ? 'selected' : ''}
//           role="option" key={p.id}>
//           {p.name}
//         </div>)}
//       </div>
//     </div>
//   )
// }

export const PlaylistList = (props: Props, { }) => {
  // const [selectedId, setSelectedId] = useState(props.selectedId)

  return (
    <div>
      <div role="listbox" className="list-group">
        {props.playlists.map(p => <div
          //  onClick={() => setSelectedId(p.id)}
          onClick={() => props.onSelected(p.id)}
          aria-selected={props.selectedId === p.id}
          className={`list-group-item ${props.selectedId === p.id ? 'selected' : ''}`}
          role="option" key={p.id}>
          {p.name}
        </div>)}
      </div>
    </div>
  )
}
