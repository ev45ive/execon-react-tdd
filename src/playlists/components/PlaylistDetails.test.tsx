import React from 'react'
import { render, RenderResult, screen, fireEvent } from '@testing-library/react'
import { PlaylistDetails } from './PlaylistDetails'
import { Playlist } from '../../common/model/playlist'
import renderer from 'react-test-renderer'

declare global {
  // https://www.typescriptlang.org/docs/handbook/declaration-merging.html
  interface Window {
    translation_map: {
      [k: string]: string
    };
  }
}

describe('PlaylistDetails', () => {
  let onEditSpy: jest.Mock<void, []>

  // beforeEach(()=>{})

  const setup = ((playlistOverrides: Partial<Playlist>) => {
    // window.translation_map = {}
    // Object.defineProperty(window, 'translation_map', {
    //   get: jest.fn(), configurable: true
    // })

    // jest.spyOn(window, 'translation_map', 'get').mockReturnValue({
    //   EDIT: 'EDYTUJ'
    // })
    onEditSpy = jest.fn()

    const playlistData: Playlist = {
      id: '123',
      name: 'My Playlist',
      public: false,
      description: 'Test description',
      ...playlistOverrides
    }
    render(<PlaylistDetails playlist={playlistData} onEdit={onEditSpy} />)

    return { onEditSpy }
  })

  it('notifies parent when "Edit" button is clicked', () => {
    // Arrange
    const { onEditSpy } = setup({})
    const btn = screen.getByRole('button', { name: 'Edit' })
    // Act
    // btn.click()
    fireEvent.click(btn, {/* extra event properities */ })
    // Assert
    expect(onEditSpy).toHaveBeenCalled()
  })

  it('should render playlist', () => {
    setup({ public: true })
    expect(screen.getByTestId('playlist_name')).toHaveTextContent(('My Playlist'))
    expect(screen.getByTestId('playlist_description')).toHaveTextContent(('Test description'))
  })

  it('Shortens description to max 30', () => {
    setup({ description: 'X'.repeat(32) })
    expect(screen.getByTestId('playlist_description').textContent).toHaveLength(30)
  })

  it('render public playlist', () => {
    setup({ public: true })
    expect(screen.getByTestId('playlist_public')).toHaveTextContent(('YES'))
    expect(screen.getByTestId('playlist_public')).toHaveStyle({ color: 'green' })
    expect(screen.getByTestId('playlist_public')).toHaveClass('playlist_public')
  })

  it('render private playlist', () => {
    setup({ public: false })
    expect(screen.getByTestId('playlist_public')).toHaveTextContent(('NO'))
    expect(screen.getByTestId('playlist_public')).toHaveStyle({ color: 'blue' })
    expect(screen.getByTestId('playlist_public')).toHaveClass('playlist_private')
  })


  it('match snapshot', () => {
    const playlistData: Playlist = {
      id: '123',
      name: 'My Playlist',
      public: false,
      description: 'Test description',
    }
    const elem = renderer.create(<PlaylistDetails playlist={playlistData} onEdit={() => { }} />)
    expect(elem).toMatchSnapshot()
  })


})
