import React, { useState } from 'react'
import { Playlist } from '../../common/model/playlist'

interface Props {
  playlist: Playlist
  onCancel(): void
  onSave(draft: Playlist): void
}

export const PlaylistEditForm = ({ playlist, onCancel, onSave }: Props) => {
  const [name, setName] = useState(playlist.name)
  const [isPublic, setIsPublic] = useState(playlist.public)
  const [description, setDescription] = useState(playlist.description)

  return (
    <div>
      <div className="form-group">
        <label htmlFor="playlist_name">Name:</label>
        <input id="playlist_name" type="text" className="form-control" value={name}
          onChange={e => setName(e.target.value)} />
        <div data-testid="name-counter">{name.length} / 170</div>
      </div>

      <div className="form-group">
        <label><input type="checkbox" checked={isPublic}
          onChange={e => setIsPublic(e.target.checked)} /> Public</label>
      </div>

      <div className="form-group">
        <label htmlFor="playlist_description">Description:</label>
        <textarea id="playlist_description" className="form-control" value={description}
          onChange={e => setDescription(e.target.value)} />
      </div>

      <button className="btn btn-danger" onClick={onCancel}>Cancel</button>
      <button className="btn btn-danger" onClick={() => {
        onSave({
          ...playlist, name
        })
      }}>Save</button>
    </div>
  )
}
