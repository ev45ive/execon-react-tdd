// tsrfac

import React from 'react'
import { Playlist } from '../../common/model/playlist'

// import translation_map from './translation_map'
// store.getData().translation_map
// window.translation_map

interface Props {
  // translation_map:{}
  playlist: Playlist,
  onEdit(): void
}

export const PlaylistDetails = ({ playlist, onEdit }: Props) => {
  return (
    <div>

      <dl>
        <dt>Name:</dt>
        <dd data-testid="playlist_name">{playlist.name}</dd>
      </dl>

      <span data-testid="playlist_public"
        className={playlist.public ? 'playlist_public' : 'playlist_private'}
        style={{ color: playlist.public ? 'green' : 'blue' }}>
        {playlist.public ? 'YES' : 'NO'}
      </span>

      <span data-testid="playlist_description">
        {playlist.description.slice(0, 30)}
      </span>

      {/* <button>translation_map['Edit']</button> */}
      {/* <button>Cancel</button> */}
      {/* <button onClick={e => onEdit(e)}>Edi</button> // Error ! */}

      <button onClick={onEdit}>Edit</button>

    </div>
  )
}
