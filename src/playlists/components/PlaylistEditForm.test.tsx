import { fireEvent, render, screen } from '@testing-library/react'
import React from 'react'
import { Playlist } from '../../common/model/playlist'
import { PlaylistEditForm } from './PlaylistEditForm'

import userEvent from '@testing-library/user-event'

describe('PlaylistEditForm', () => {

  const playlistData: Playlist = {
    id: '123',
    name: 'My Playlist',
    public: false,
    description: 'Test description',
  }

  const setup = () => {
    const onCancel = jest.fn<void, []>();
    const onSave = jest.fn<void, [Playlist]>();

    render(<PlaylistEditForm
      playlist={playlistData}
      onCancel={onCancel}
      onSave={onSave} />)

    return { onCancel, onSave }
  }

  it('shows form with playlist data from parent', () => {
    setup()

    const inputElem = screen.getByLabelText('Name:', { exact: true })
    expect(inputElem).toHaveValue(playlistData.name)


    const publicCheckbox = screen.getByLabelText('Public', { trim: true })
    expect(publicCheckbox).not.toBeChecked()

    const descrElem = screen.getByLabelText('Description:', { exact: true })
    expect(descrElem).toHaveValue(playlistData.description)
  })

  it('show playlist current name length counter (i.e. 12 / 170)', () => {
    setup()
    const counter = screen.getByTestId('name-counter')
    expect(counter).toHaveTextContent(`${'My Playlist'.length} / 170`)

    const inputElem = screen.getByLabelText('Name:', { exact: true })
    userEvent.clear(inputElem)
    expect(counter).toHaveTextContent(`0 / 170`)
    userEvent.type(inputElem, 'Alice has a cat', {/* delay:1 */ })
    expect(counter).toHaveTextContent(`${'Alice has a cat'.length} / 170`)

  })

  it('nofify parent when "Cancel" clicked', () => {
    const { onCancel } = setup()

    userEvent.click(screen.getByRole('button', { name: 'Cancel' }))

    expect(onCancel).toHaveBeenCalled()
  })

  it('send updated data to parent when "Save" clicked', () => {
    const { onSave } = setup()

    const inputElem = screen.getByLabelText('Name:', { exact: true })
    userEvent.clear(inputElem)
    userEvent.type(inputElem, 'Alice has a cat', {/* delay:1 */ })

    userEvent.click(screen.getByRole('button', { name: 'Save' }), {})

    expect(onSave).toHaveBeenCalledWith({
      ...playlistData,
      name: 'Alice has a cat'
    })

  })


})
