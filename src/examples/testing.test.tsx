import React from 'react';
export { }

it('should test truthiness of true', () => {
  expect(true).toBe(true)
})

describe('Example Tests', () => {

  it('should test truthiness of true', () => {
    expect(true).toBe(true)
    // expect({v:true}).toBe({v:true}) // false
    expect({ v: true }).toEqual({ v: true }) // true
  })

  it('checks arrayt contains', () => {
    const arr = [1, 2, 3]
    expect(arr).toContain(2)

    const arr2 = [{ v: 1 }, { v: 2 }, { x: 3 }]
    expect(arr2).toContainEqual({ v: 2 })
    expect(arr2).toContainEqual({ x: expect.anything() })
    expect(arr2).toContainEqual({ v: expect.any(Number) })
    arr2.forEach(() => {
      expect(arr2[0]).toHaveProperty('v'/* ,'123' */)
      expect(arr2[0]).toMatchObject({
        // active: true,
        // data: expect.any(Array),
        // ID: expect.any(String)
    })
    })
  })

/* 
  thrown: "Exceeded timeout of 5000 ms for a test.
    Use jest.setTimeout(newTimeout) to increase the timeout value, if this is a long-running test." */

  it('async tests', (done) => {
    setTimeout(() => {
      expect(false).not.toBeTruthy()
      done()
    })
  })

})


function add(a: number, b: number) { return a + b }

describe('Math functions', () => {

  describe('Add', () => {
    it('should add numbers', () => {
      expect(add(1, 2)).toEqual(3)
    })
    it('should add negative numbers', () => {
      expect(add(-1, -2)).toEqual(-3)
    })
  })

})