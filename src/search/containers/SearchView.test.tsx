import { act, render, screen } from '@testing-library/react'
import React from 'react'
import { mocked } from 'ts-jest/utils'
import { Album, SimpleAlbum } from '../../common/model/search'
import { searchAlbums } from '../../common/SearchAPI'
import { SearchForm } from '../components/SearchForm'
import { SearchResults } from '../components/SearchResults'

import { SearchView } from './SearchView'
jest.mock('../components/SearchForm')
jest.mock('../../common/SearchAPI')
jest.mock('../components/SearchResults')

describe('SearchView', () => {

  const setup = () => {
    mocked(SearchForm).mockReturnValue(<div data-testid="SearchFormMOCK" />)
    mocked(SearchResults).mockReturnValue(<div data-testid="SearchResultsMOCK" />)
    render(<SearchView />)
  }

  it('shows empty search results', () => {
    setup()
    expect(screen.getByTestId('SearchResultsMOCK')).toBeInTheDocument()
    expect(SearchResults).toHaveBeenCalledWith({
      results: []
    }, {})
  })

  it('shows empty search form', () => {
    setup()
    expect(screen.getByTestId('SearchFormMOCK')).toBeInTheDocument()

  })

  it('fetch data when form is submitted and show results', async () => {
    setup()
    const mockData = [
      { id: '123', name: 'Test123', images: [{ url: 'test' }] }
    ] as unknown as SimpleAlbum[]

    // Create resolved promise mock
        const promise = Promise.resolve(mockData as Album[])
    mocked(searchAlbums).mockImplementation(() => promise)

    // Call function to load mock promise into component
    mocked(SearchForm).mock.calls[0][0].onSearch('batman')
    expect(searchAlbums).toHaveBeenCalledWith('batman')

    // Wait for component to render AFTER promise resolved
    await act(() => promise as unknown as Promise<void>)

    // Check what was 'rendered' in child component:
    expect(mocked(SearchResults)).toHaveBeenLastCalledWith({
      results: mockData
    }, {})
  });

})
