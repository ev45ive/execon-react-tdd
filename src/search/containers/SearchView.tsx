
import React, { useState } from 'react'
import { SimpleAlbum } from '../../common/model/search'
import { searchAlbums } from '../../common/SearchAPI'
import { SearchForm } from '../components/SearchForm'
import { SearchResults } from '../components/SearchResults'

interface Props {

}

export const SearchView = (props: Props) => {
  const [query, setQuery] = useState('')
  const [results, setResults] = useState<SimpleAlbum[]>([])

  const search = (query: string) => {
    searchAlbums(query).then(albums => {
      setResults(albums)
    })
  }

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm query={query} onSearch={search} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          <SearchResults results={results} />
        </div>
      </div>

    </div>
  )
}
