import React, { useState } from 'react'

interface Props {
  query: string
  onSearch(query: string): void
}

export const SearchForm = (props: Props) => {
  const [query, setQuery] = useState(props.query)
  return (
    <div>
      <div className="input-group mb-3">
        <input type="text" className="form-control" placeholder="Search" value={query}
          onChange={(e) => setQuery(e.target.value)} />

        <span className="input-group-text" onClick={() => props.onSearch(query)}>Search</span>
      </div>
    </div>
  )
}
