import React from 'react'
import { SimpleAlbum } from '../../common/model/search'

interface Props {
  results: SimpleAlbum[]
}

export const SearchResults = ({ results }: Props) => {
  return (
    <div>
      <div className="row row-cols-2 row-cols-md-4 g-0">
        {results.map(result =>
          <div className="col">
            <div className="card">
              <img src={result.images[0].url} className="card-img-top" />
              <div className="card-body">
                <h5 className="card-title">{result.name}</h5>
              </div>
            </div>
          </div>)}
      </div>
    </div>
  )
}
