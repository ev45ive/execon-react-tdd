import { Album } from "./model/search"



export const searchAlbums = async (query: string): Promise<Album[]> => {

  var client_id = '224f8748135846bb8e4051449f2fe117';
  var client_secret = '6ba921f038c34c2dbf62e5450042c32d';
  const token = await (await fetch('https://accounts.spotify.com/api/token', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': `Basic ${btoa(`${client_id}:${client_secret}`)}`
    },
    body: new URLSearchParams({ grant_type: 'client_credentials' }).toString()

  })).json()

  const result = await (await fetch('https://api.spotify.com/v1/search?type=album&q=' + query, {
    headers: { Authorization: 'Bearer ' + token.access_token }
  })).json()

  return (result as any).albums.items;
}


// function(prevState,action):nextState
// epxect(nextSate).toEq({})

// <Provider store={mockStore}>
//   <AnyReduxCOmponent/>
// </Provider>