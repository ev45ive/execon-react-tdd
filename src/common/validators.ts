export class ValidatorsAPIRequests {

  static checkIsAvailable(field: Field): Promise<ErrorResult | null> {
    throw new Error('Not Implmented')
  }
}

// const x:Array<number> = [1,2]

type Field = {
  name: string;
  value: string;
};

type ErrorResult = {
  error: string;
};

export class Validators {

  static asyncValidateAlreadyExists(
    field: Field,
    callback: (result: ErrorResult | null) => void) {

    ValidatorsAPIRequests
      .checkIsAvailable(field)
      .then(result => {
        callback(result)
      })
  }

  static validateMinlength(minLength: number) {
    return (field: Field) => {
      return field.value.length === 0 || field.value.length >= minLength ? null : {
        error: `Minimal length of ${field.name} is ${minLength} characters`
      }
    }
  }

  static validateRequired({ value, name }: Field) {
    return value ? null : {
      error: `${name} is required`
    }
  }

}