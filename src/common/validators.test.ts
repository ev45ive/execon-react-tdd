import { Validators, ValidatorsAPIRequests } from "./validators";


describe('Playlist Validators', () => {

  describe('Name already exists validator', () => {
    it('Valiadator API integrtaion', (done) => {
      // Arrange - Given...
      const spy = jest.spyOn(ValidatorsAPIRequests, 'checkIsAvailable');
      // spy.mockImplementation((params)=>return value)
      // spy.mockImplementation(()=>Promise.resolve(null))
      spy.mockResolvedValue({ error: 'Some fake error' })

      // Act - When...
      Validators.asyncValidateAlreadyExists({
        name: 'Name',
        value: 'admin'
      }, (result: { error: string } | null) => {
        expect(result).toEqual({ error: 'Some fake error' })
        done()
      })

      // Assert - Then...
      // spy.mock.calls[0][0]
      // spy.mock.results[0]
      expect(spy).toHaveBeenCalledWith({
        name: 'Name',
        value: 'admin'
      })
    })
  })


  describe('MinLength Validator', () => {
    // Validators.validateMinlength(3) => Validators(field)

    // it('returns validator function',()=>{})
    it('returns error for value shorter than x', () => {
      const validator = Validators.validateMinlength(3)
      expect(validator({ value: '12', name: '' })).toEqual({
        error: expect.any(String)
      })
      const validator2 = Validators.validateMinlength(4)
      expect(validator2({ value: '123', name: '' })).toEqual({
        error: expect.any(String)
      })
    })

    it('returns ok for value longer than x', () => {
      const validator = Validators.validateMinlength(3)
      expect(validator({ value: '123', name: '' })).toEqual(null)
      const validator2 = Validators.validateMinlength(4)
      expect(validator2({ value: '1234', name: '' })).toEqual(null)
    })

    it('ignores empty / optional fields', () => {
      const validator = Validators.validateMinlength(3)
      expect(validator({ value: '', name: '' })).toEqual(null)
    })

    it('shows error message with field name and required length', () => {
      const resultWithName = Validators.validateMinlength(3)({
        value: '12', name: 'TestNAME'
      });
      expect(resultWithName).toEqual({ error: 'Minimal length of TestNAME is 3 characters' })
    })
  })

  describe('Required Validator', () => {

    it('shows error when name is empty', () => {
      const resultErr = Validators.validateRequired({
        value: '', name: 'Name'
      });
      expect(resultErr).toEqual({ error: 'Name is required' })
    });

    it('shows ok when name is not empty', () => {
      const resultOK = Validators.validateRequired({
        value: 'Alice', name: 'TestNAME'
      });
      expect(resultOK).toEqual(null)
    })

    it('shows error message with field name', () => {
      const resultWithName = Validators.validateRequired({
        value: '', name: 'TestNAME'
      });
      expect(resultWithName).toEqual({ error: 'TestNAME is required' })
    })
  });
});